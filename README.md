# Commands #

Terminal commands for linux and other operating system.

### Install PhpStorm with snapd ###

* Install spapd: sudo apt install snapd
* Insall PhpStrom : sudo snap install phpstorm --classic

### LAMP ###

* Start Apache2: sudo /etc/init.d/apache2 start
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

### Laravel path ###
* export PATH="~/.composer/vendor/bin:$PATH" 
* reload : source ~/.bashrc 
* verify : echo $PATH

### API Response Messages ###

* {"errors":[{"message":"The provided authorization grant is invalid, expired, or revoked","field":null,"help":null}]}
